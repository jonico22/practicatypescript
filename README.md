#PRACTICA01

// EJERCICIO 1:
var info = {
name: 'Tony',
lastname: 'Stark',
nickname: 'Iroman'
}
/_
Crear una funcion que reciba 3 parametros (name, lastname y nickname)
El resultado debe mostrar por consola el texto "Me llamo Tony Stark y soy Iroman"
_/

// EJERCICIO 2
var info = {
name: 'Clark',
lastname: 'Kent',
nickname: 'Superman'
}
var user = {
name: info.name,
lastname: info.lastname,
nickname: info.nickname,
allPowers: ['Superfuerza', 'Volar', 'Velocidad', 'Telepatia']  
}
/_
Crear una funcion de tipo promesa que reciba el arreglo de poderes y devuelva un nuevo formato
Como resultado hacer un console.log(user) debe mostrar lo siguiente.
{
name: 'Clark',
lastname: 'Kent',
allPowers: [
{ power: 'Superfuerza' },
{ power: 'Volar' },
{ power: 'Velocidad' },
{ power: 'Telepatia' },
]
}
_/

// EJERCICIO 3
/_
Crear una clase llamada Figura con propiedades base y altura
Los valores de base y altura deben ser inicializadas a traves del constructor
La clase debe tener un metodo llamado getAreaTriangulo, esta funcion debera calcular el area de un triangulo
_/

// EJERCICIO 4
var datos = [
{ name: 'Kelvin', age: 29, country: 'Perú' },
{ name: 'Milagros', age: 26, country: 'Colombia' },
{ name: 'Luis', age: 28, country: 'Colombia' },
{ name: 'Ruben', age: 29, country: 'Brasil' },
{ name: 'Andrea', age: 28, country: 'Argentina' },
]
/\*
Resultados

1. Mostrar el objeto que tenga como propiedad edad 29
2. Mostrar en un arreglo, todas las propiedades tiene el objeto del punto 1
3. Mostrar un arreglo de solo edades
4. Del arreglo del punto 3, mostrar por consola un nuevo arreglo pero sin duplicados
5. Del arreglo del punto 3, mostrar por consola si existe o no el numero 30
   \*/
